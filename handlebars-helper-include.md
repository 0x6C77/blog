Nest templates within templates

```js
/* Add include helper to handlebars */
Handlebars.registerHelper('include', function(template, options) {
    var context = options || this;
    template = Handlebars.compile(config.templates[template]);    
    return template(context);
});
```

### Usage
```
{{include 'foo'}}
```

Optionally pass in a context as the second parameter:
```
{{include 'foo' bar}}
```