# Creating a custom Docker image to use with GitLab CI

Docker commands may need to be run as root. Append sudo or login as root if you get `permission denied while trying to connect to the Docker daemon socket`.

## Create Docker file
You can refer to an example that builds an enviroment with PHP, composer, Node.js, npm, SSH and rsync:
`https://gitlab.com/DefendTheWeb/DefendTheWeb/blob/stage/.gitlab/Dockerfile`


## Build Docker image
Build

```
cd .gitlab
docker build -t registry.gitlab.com/[project]/[image] .
```

Note: The tag needs to be lowercase

## Upload to GitLab registry
Create a [personal access](https://gitlab.com/profile/personal_access_tokens) token within GitLab with the api scope. You will use this to login to the GitLab Docker registry.

```
docker login -u [username] -p [access_token] registry.gitlab.com
docker push registry.gitlab.com/[project]/[image]
```

## Use in CI
Now the Docker image is successfully built and in the GitLab repository we can start using it in our CI script. Simply define the `image` in your `.gitlab-ci.yml`.

```
image: registry.gitlab.com/[project]/[image]:latest
```