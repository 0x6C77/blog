# Matrix login with `m.login.token`

# Macroons
A new authorization credential developed by Google - [Macaroons: Cookies with Contextual Caveats for Decentralized Authorization in the Cloud](https://ai.google/research/pubs/pub41892). Like cookies they are barer tokens and can be used to authorize someone with a valid macaroon. You can read a full breakdown [here](http://hackingdistributed.com/2014/05/16/macaroons-are-better-than-cookies/).

# Setup
Within your `homeserver.yaml` a `macaroon_secret_key` needs to be set.



# Testing
After a lot of failed attempts throwing macaroons at Synapse I put together some test code to quickly validate macaroons. It was made to replicate the same spec Synapse uses on the server.

```python
import pymacaroons
import time

token = "..."
macaroon = pymacaroons.Macaroon.deserialize(token)

user_id = ''
user_prefix = "user_id = "
for caveat in macaroon.caveats:
    if caveat.caveat_id.startswith(user_prefix):
        user_id = caveat.caveat_id[len(user_prefix):]

v = pymacaroons.Verifier()

# the verifier runs a test for every caveat on the macaroon, to check
# that it is met for the current request. Each caveat must match at
# least one of the predicates specified by satisfy_exact or
# specify_general.
v.satisfy_exact("gen = 1")
v.satisfy_exact("type = " + 'login')
v.satisfy_exact("user_id = %s" % user_id)
v.satisfy_exact("guest = true")

# verify_expiry should really always be True, but there exist access
# tokens in the wild which expire when they should not, so we can't
# enforce expiry yet (so we have to allow any caveat starting with
# 'time < ' in access tokens).
#
# On the other hand, short-term login tokens (as used by CAS login, for
# example) have an expiry time which we do want to enforce.

def verify_expiry(caveat):
    prefix = "time < "
    if not caveat.startswith(prefix):
        return False
    expiry = int(caveat[len(prefix):])
    now = int(round(time.time() * 1000))
    return now < expiry

if verify_expiry:
    v.satisfy_general(verify_expiry)
else:
    v.satisfy_general(lambda c: c.startswith("time < "))

# access_tokens include a nonce for uniqueness: any value is acceptable
v.satisfy_general(lambda c: c.startswith("nonce = "))

print(macaroon.inspect())

if v.verify(macaroon, "NUOqiKewmKyRC1NO_ug"):
    print "Macaroon is valid"
else:
    print "Macaroon is invalid"
```