If I configure the external monitor as sitting above the laptop in arandr, my mouse cursor turns huge when I open apps. However, If i configure them side by side, the cursor remains the same size.
 
~/.Xresources
```
Xcursor.size: 16
```