```
mkdir -p ~/.local/share/file-manager/actions
nano ~/.local/share/file-manager/actions/rootedit.desktop
```

```
[Desktop Entry]
Type=Action
Name=Compress...
Profiles=profile-zero;

[X-Action-Profile profile-zero]
Exec=file-roller -d %F
Name=Default profile
```