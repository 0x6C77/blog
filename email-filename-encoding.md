Email attachment files are sometimes encoded, especially when using a wider character set.

For example:
```
=?utf-8?B?ZmlsZW5hbWUudHh0?=
```

The filename is encoded using `?` as a delimeter. So it's easiest to split it into three distinct parts:
```
utf-8 // Character encoding
B // Denotes the filename is base64 encoded
ZmlsZW5hbWUudHh0 // Encoded filename
```

Base64 decoding the third value gives us the files name `filename.txt`.